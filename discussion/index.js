console.log("Introduction to JSON")

// JSON Objects
/*
	- JSON stands for JavaScript Object Notation
	- JSON is also used for other programming languages hence the name Javscript Object

	Syntax:
		{
			"propertyA": "valueA",
			"propertyB": "valueB",
		}
*/

// JSON Object

/*{
	"city": "Quezon City",
	"province": "Metro Manila",
	"country": "Philippines"
}*/


// JSON Array

/*"cities" = [
	{"city": "Quezon City", "province": "Metro Manila", "country": "Philippines"},
	{"city": "Manila", "province": "Metro Manila", "country": "Philippines"},
	{"city": "Makati City", "province": "Metro Manila", "country": "Philippines"}	
]*/


// JSON Methods
/*
	The JSON Object contains methods for parsing and converting data into stringified JSON
*/


// Converting Data into stringified JSON

let batchesArr = [
	{batchName: "Batch 197"},
	{batchName: "Batch 198"}
]

console.log(batchesArr)

// The stringify method is used to convert JS Objects into string
// We are doing this before sending the date to convert and array or an object to its string equivalent.

console.log("Result from stringify method:")
console.log(JSON.stringify(batchesArr))

let userProfile = JSON.stringify({
	name: "John", 
	age: 31,
	address: {
		city: "Manila",
		region: "Metro Manila",
		country: "Philippines"
	}
})
console.log("Result from stringify method (object):")
console.log(userProfile)

console.log(" ")

// User Details
let firstName = prompt("What is your first name?")
let lastName = prompt("What is your last name?")
let age = prompt("What is your age?")
let address = {
	city: prompt("Which city do you live in?"),
	country: prompt("Which country does your city address belong to?")
}

let userData = JSON.stringify({
	firstName: firstName,
	lastName: lastName,
	age: age,
	address: address
})

console.log(userData)

// Convert stringified JSON into JS Objects
// JSON.parse()

console.log(" ")

let batchesJSON = `[
	{
		"batchName": "Batch 197"
	},
	{
		"batchName": "Batch 198"
	}
]`

console.log(batchesJSON)

// Upon receiving data, the JSON text can be converted into JS Objects so that we can use it in our program

console.log("Result from parse method: ")
console.log(JSON.parse(batchesJSON))


let stringifiedObject = `{
	"name": "Ivy",
	"age": "18",
	"address": {
		"city": "Caloocan City",
		"country": "Philippines"
	}}
`

console.log(stringifiedObject)
console.log("Result from parse method (object): ")
console.log(JSON.parse(stringifiedObject))